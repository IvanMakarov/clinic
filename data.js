export default {
  head: {
    lang: 'ru',
    title: 'Альверум',
    base: 'doctor.zolotarev-studio.ru'
  },

  contacts: {
    tel: "+74951234567"
  },

  mainSections: [
    {
      title: 'Услуги',
      name: '/services',
      subsections: [
        {
          title: "Гинекология",
          link: "/services/service"
        },
        {
          title: "Ведение беременности",
          link: "/services/service"
        },
        {
          title: "УЗИ",
          link: "/services/service"
        },
        {
          title: "Терапия",
          link: "/services/service"
        },
        {
          title: "Маммология",
          link: "/services/service"
        },
        {
          title: "Кардиология",
          link: "/services/service"
        },
        {
          title: "Анализы",
          link: "/services/service"
        }              
      ]
    },
    {
      title: 'Анализы',
      name: '/tests'
    },
    {
      title: 'Врачи',
      name: '/doctors'
    },
    {
      title: 'О нас',
      name: '/about'
    },
    {
      title: 'Блог',
      name: '/blog'
    },
    {
      title: 'Наши акции',
      name: '/promo',
      highlight: true
    },
    {
      title: 'Отзывы',
      name: '/reviews'
    },
    {
      title: 'Контакты',
      name: '/contacts'
    }
  ],

  services: [
    {
      title: "Гинекология",
      icon: "uterus",
      link: "/services/service"
    },
    {
      title: "Ведение беременности",
      icon: "fetus",
      link: "/services/service"
    },
    {
      title: "УЗИ",
      icon: "kidneys",
      link: "/services/service"
    },
    {
      title: "Терапия",
      icon: "muscle",
      link: "/services/service"
    },
    {
      title: "Маммология",
      link: "/services/service"
    },
    {
      title: "Кардиология",
      link: "/services/service"
    },
    {
      title: "Анализы",
      link: "/tests"
    }
  ],

  social: [
    {
      link: '',
      icon: 'fb',
      img: 'placeholder.jpg'
    },
    {
      link: '',
      icon: 'inst',
      img: 'placeholder.jpg'
    },
    {
      link: '',
      icon: 'youtube',
      img: 'placeholder.jpg'
    }
  ],

  rules: [
    "А те я все уж перетаскал, сложил штабельком между двух яблонь, там справа.",
    "Там как раз мешать не будут.",
    "А как за низ принялся — всё гнилое.",
    "Вот что значит низина.",
    "Там вода ещё щас стоит, конечно оно всё и прело а как же."
  ],

  helpers: {
    formatTel: (tel) => {
      const telFormatted = tel[0] == '+' ? `${tel.substr(0, 2)} ${tel.substr(2, 3)} ${tel.substr(5, 3)}-${tel.substr(8, 2)}-${tel.substr(10, 2)}` : `${tel[0]} ${tel.substr(1, 3)} ${tel.substr(4, 3)}-${tel.substr(7, 2)}-${tel.substr(9, 2)}`;
      return telFormatted
    },
    // faker: faker
  },

  map: {
    center: [55.721678, 37.815098],
    marker: [55.721678, 37.815098],
    markerText: 'Клиника'
  },

  footer: {
    links: [
      {
        link: '/services',
        text: 'Услуги'
      },
      {
        link: '/tests',
        text: 'Анализы'
      },
      {
        link: '/doctors',
        text: 'Врачи'
      },
      {
        link: '/about',
        text: 'О нас'
      },
      {
        link: '/blog',
        text: 'Блог'
      },
      {
        link: '/promo',
        text: 'Акции'
      },
      {
        link: '/reviews',
        text: 'Отзывы'
      },
      {
        link: '/contacts',
        text: 'Контакты'
      },
      {
        link: '/price',
        text: 'Прайс'
      }
    ]
  },

  misc: {
    name: 'John Doe',
    word: 'Heading',
    sentence: 'Я взял три штуки рубироида, а потом ещё две — пригодятся, потом двадцатки выписал на сто рублей, так что теперь всё в норме.',
    paragraph: 'А я тут разобрал почти весь сарай, так что он стоит весь пустой, будто его и не было. С боков доска отличная, не гнилая, я её сложил на солнце — пусть посушится и тогда в штабель с новыми. А те я все уж перетаскал, сложил штабельком между двух яблонь, там справа. Там как раз мешать не будут.',
  },

  forms: [
    {
      id: 'callback',
      title: {
        main: 'Перезвоните мне'
      },
      btn: 'Перезвоните мне',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "tel",
          placeholder: "Телефон",
          required: true,
          name: 'tel'
        },
        {
          input: "input",
          type: "text",
          placeholder: "Когда вам перезвонить?",
          required: false,
          name: 'time'
        }
      ]
    },
    {
      id: 'appointments',
      title: {
        main: 'Запись на прием'
      },
      btn: 'Записать меня',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "tel",
          placeholder: "Телефон",
          required: true,
          name: 'name'
        },
        {
          input: "select",
          required: false,
          name: 'reason',
          label: 'Цель визита',
          options: [
            'Гинекология',
            'Маммология',
            'Кардиология'
          ]
        },
        {
          input: "date",
          name: "date"
        }
      ]
    },
    {
      id: 'appointments-tests',
      title: {
        main: 'Запись на анализы'
      },
      btn: 'Записать меня',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "tel",
          placeholder: "Телефон",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "text",
          placeholder: "Когда вам перезвонить?",
          required: false,
          name: 'time'
        }
      ]
    },
    {
      id: 'feedback',
      title: {
        main: 'Написать директору', 
        subtitle: 'Напишите пожелание или претензию'
      },  
      btn: 'Отправить',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "email",
          placeholder: "Email",
          required: true,
          name: 'email'
        },
        {
          input: "textarea",
          placeholder: "Сообщение",
          required: false,
          name: 'message'
        }
      ]
    },
    {
      id: 'review',
      title: {
        main: 'Оставить отзыв',
        // subtitle: 'Напишите пожелание или претензию'
      },  
      btn: 'Отправить',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "email",
          placeholder: "Email",
          required: false,
          name: 'email'
        },
        {
          input: "textarea",
          placeholder: "Отзыв",
          required: false,
          name: 'message'
        }
      ]
    },
    {
      id: 'application',
      title: {
        main: 'Отклик на вакансию',
        // subtitle: 'Напишите пожелание или претензию'
      },  
      btn: 'Отправить',
      items: [
        {
          input: "input",
          type: "text",
          placeholder: "Имя",
          required: true,
          name: 'name'
        },
        {
          input: "input",
          type: "tel",
          placeholder: "Телефон",
          required: false,
          name: 'tel'
        },
        {
          input: "textarea",
          placeholder: "Описание",
          required: false,
          name: 'message'
        }
      ]
    }
 ],

  prices: [
    {
      title: 'Какие-то услуги',
      items: [
        {
          title: 'Услуга 1',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 2',
          link: '',
          price: '3000',
        }
      ]
    },
    {
      title: 'Еще какие-то услуги',
      items: [
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        }
      ]
    },
    {
      title: 'Еще какие-то услуги',
      items: [
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        },
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        },
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        }
      ]
    },
    {
      title: 'Еще какие-то услуги',
      items: [
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        }
      ]
    },
    {
      title: 'Еще какие-то услуги',
      items: [
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        },
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        },
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        }
      ]
    },
    {
      title: 'Еще какие-то услуги',
      items: [
        {
          title: 'Услуга 3',
          link: '',
          standout: true,
          price: '1000',
          priceOld: '1200',
        },
        {
          title: 'Услуга 4',
          link: '',
          price: '3000',
        }
      ]
    }
  ],
  testPrices: {
    title: 'Анализы',
    items: [
      {
        title: 'Анализ 1',
        link: 'test',
        standout: false,
        price: '1000',
        priceOld: '1200',
      },
      {
        title: 'Aнализ 2',
        link: 'test',
        price: '3000',
      }
    ]
  },
  staff: [
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    },
    {
      link: './doctor',
      img: '../img/doctor.jpg',
      name: 'Константин Константинович Константинопольский',
      position: 'Ведущий оториноларинголог-уринотерапевт'
    }
  ],
  select: {
    options: [
      "Выбор 1",
      "Выбор 2",
      "Выбор 3",
      "Выбор 4",
      "Выбор 5",
      "Выбор 6",
      "Выбор 7"
    ] 
  },
  infoBlock: [
    {
      link: '',
      text: 'Для тех'
    },
    {
      link: '',
      text: 'Для этих'
    },
    {
      link: '',
      text: 'Для таких'
    },
    {
      link: '',
      text: 'Для сяких',
      submenu: [
        {
          link: 'a',
          text: 'Нечто 1'
        },
        {
          link: 'a',
          text: 'Нечто 2'
        },
        {
          link: 'a',
          text: 'Нечто 3'
        },
        {
          link: 'a',
          text: 'Нечто 4'
        },
        {
          link: 'a',
          text: 'Нечто 5'
        }
      ]
    },
    {
      link: '',
      text: 'Для пятых'
    },
    {
      link: '',
      text: 'Для десятых'
    }
  ],

  promo: [
    {
      text: 'Акция',
      link: 'placeholder'
    },
    {
      text: 'Акция c длинным названием',
      link: 'placeholder'
    },
    {
      text: 'Акция с очень длинным названием',
      link: 'placeholder'
    },
    {
      text: 'Акция с очень длинным названием от ведущего отриноларинголога-уринотерапевта Константина Константиновича Константинопольского',
      link: 'placeholder'
    },
    {
      text: 'Акция',
      link: 'placeholder'
    },
    {
      text: 'Акция',
      link: 'placeholder'
    },
    {
      text: 'Акция',
      link: 'placeholder'
    },
    {
      text: 'Акция',
      link: 'placeholder'
    },

  ],

  clinics: [
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
    {
      name: 'Клиника оториноларингологии-уринотерапии Константина Константинопольского',
      city: 'г. Усть-Каменногорск',
      address: 'ул. Жугдэрдэмидийна Гуррагчи, д. 666/13',
      tel: '8-800-755-35-35',
      email: 'ust-kam@alverum.com',
      hours: [
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        },
        {
          days: 'пн-пт',
          start: '10:00',
          end: '19:00'
        }        
      ]
    },
  ],

  features: [
    {
      standout: '10 000',
      regular: 'сеансов уринотерапии'
    },
    {
      standout: '10 000',
      regular: 'сеансов уринотерапии'
    },
    {
      standout: '10 000',
      regular: 'сеансов уринотерапии'
    },
    {
      standout: '10 000',
      regular: 'сеансов уринотерапии'
    }
  ],

  history: [
    {
      year: '1999'
    },
    {
      year: '2001'
    },
    {
      year: '2005'
    },
    {
      year: '2007'
    },
    {
      year: '2009-'
    }
  ],

  vacancies: [
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: '/about/careers/vacancy'
    },
    {
      title: 'Оториноларинголог-уринотерпевт',
      salary: '10 000',
      link: ' /about/careers/vacancy'
    }
  ]
}