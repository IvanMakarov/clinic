import scroll from 'smoothscroll' 

export default () => {
  const btn = document.querySelector('.index-slider-keep-scrolling'); 
  const to = document.querySelector('#services');

  btn.addEventListener('click', () => {
    scroll(to)
  });  
}