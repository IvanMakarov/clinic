export default (headerSelector, sliderSelector) => {
  const header = document.querySelector(headerSelector)
  const slider = document.querySelector(sliderSelector)
  if (header) {
    window.addEventListener('scroll', updateHeaderPosition)   
  }

  function updateHeaderPosition() {
    const height = window.innerHeight
    const scroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
    const offset = header.getBoundingClientRect().height

    function fixToTop() {
      if (!header.classList.contains('js-fixed')) {
        header.classList.add('js-fixed')
      }
    }

    function fixToContent() {
      if (header.classList.contains('js-fixed')) {
        header.classList.remove('js-fixed')
      }
    }

    function hideSlider() {
      slider.classList.add('hidden')
    }

    function showSlider() {
      slider.classList.remove('hidden')
    }

    if (scroll > height - offset) {
      fixToTop()
      hideSlider()
    } else {
      fixToContent()
      showSlider()
    } 
  }

  $('.index-slider').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 1500,
    margin: 0,
    nav: true,
    dots: true,
    navContainer: '.index-slider-nav',
    dotsContainer: '.index-slider-dots',
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      650: {
        items: 1
      },
      900: {
        items: 1
      },
      1200: {
        items: 1
      }
    }
  })
}

