import map from '../includes/map/map'
import slider from '../includes/slider/slider'
import nav from '../includes/nav/nav'
import modal from '../includes/modals/modals'
import expandTextareas from './textarea'
import indexSlider from './index-slider'
import smoothScroll from './smoothScroll'

map()
slider()
nav('.nav__list', '.nav-btn', 750)
modal()
expandTextareas()
indexSlider('.index-page .header', '.index-slider-container')
smoothScroll()