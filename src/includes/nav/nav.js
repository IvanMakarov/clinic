// import VanillaModal from 'vanilla-modal'

// export default () => {
//   const modal = new VanillaModal({
    
//   })
// }

export default (menuSelector, btnSelector, threshold) => {
  const menu = document.querySelector(menuSelector);
  const btn = document.querySelector(btnSelector);

  const links = [...menu.querySelectorAll('a')];
  
  btn.addEventListener('click', toggleMenu)
  
  function toggleMenu(e) {
    e.preventDefault();
    isOpen(menu) ? close(menu) : open(menu)
  }

  function close(item) {
    item.style.display = "none"
  }

  function open(item) {
    item.style.display = "flex"
  }

  function isOpen(item) {
    return item.style.display === "flex"
  }

  links.forEach(link => {
    link.addEventListener('click', () => {
      if(window.innerWidth <= threshold) {
        close(menu)
      }   
    })
  })

  window.addEventListener('resize', adjustMenu)

  function adjustMenu() {
    window.innerWidth > threshold ? open(menu) : close(menu)
  }
}