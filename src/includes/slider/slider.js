import sliders from './sliders'

export default () => {
  sliders.sliders.forEach(slider => {
    $(`.${slider.class}`).owlCarousel({
      loop: Boolean(slider.loop),
      autoplay: false,
      autoplayHoverPause: true,
      autoplaySpeed: 1500,
      margin: slider.margin,
      nav: true,
      navContainer: `.${slider.navClass}`,
      navText: ['', ''],
      responsive: {
        0: {
          items: slider.itemsXS
        },
        650: {
          items: slider.itemsS
        },
        900: {
          items: slider.itemsMD
        },
        1200: {
          items: slider.itemsLG
        }
      }
    })
  })
}